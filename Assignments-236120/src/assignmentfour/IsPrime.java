package assignmentfour;

import java.util.Scanner;

public class IsPrime {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int a, temp;
		boolean isPrime = true;
		
		System.out.print("Enter an integer: ");
		a = sc.nextInt();
		
		for(int i = 2; i<a/2; i++) {
			temp = a%2;
			if(temp == 0) {
				isPrime = false;
				break;
			}
		}
		
		if(isPrime == true) {
			System.out.println(a + " is a prime number.");
		}
		else {
			System.out.println(a + " is not a prime number.");
		}
	}
}