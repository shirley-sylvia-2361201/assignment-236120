package assignmentfour;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class IterateArrList {
	
	public static void main(String[] args) {
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		//list.add(100);
		//list.add(200);
		//list.add(300);
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter 3 element: ");
		for(int i = 0; i<3; i++) {
			list.add(sc.nextInt());
		}
		
		System.out.println("\nSize of Array List is: " + list.size());
		
		Iterator<Integer> itr = list.iterator();
		System.out.print("While Loop: ");
		while(itr.hasNext()) {
			System.out.print(itr.next() + " ");
		}
		
		System.out.print("\nFor Each Loop: ");
		for (Object obj: list) {
			System.out.print(obj + " ");
		}
		
		System.out.print("\nFor loop: ");
		for(int i=0;i<list.size();i++) {
			System.out.print(list.get(i) + " ");
		}
		
	}

}
