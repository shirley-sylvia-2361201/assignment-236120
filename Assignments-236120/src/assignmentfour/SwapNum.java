package assignmentfour;

import java.util.Scanner;

public class SwapNum {
	
	int arr[] = new int[2];
	
	public void swap(){		
		int temp = arr[0];
		arr[0] = arr[1];
		arr[1] = temp;	
		
		System.out.println("\nInt X after: " + arr[0]);
		System.out.println("Int Y after: " + arr[1]);
	}
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		SwapNum sn = new SwapNum();
		
		System.out.println("Enter integer X and Y:");
		for(int i=0;i<sn.arr.length;i++) {
			sn.arr[i] = sc.nextInt();
		}
		
		System.out.println("\nInt X before: " + sn.arr[0]);
		System.out.println("Int Y before: " + sn.arr[1]);
		
		sn.swap();
		
		
	}

}
