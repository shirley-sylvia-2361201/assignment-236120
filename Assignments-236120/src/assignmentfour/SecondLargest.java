package assignmentfour;

public class SecondLargest {
	
	public static void main(String[] args) {
		
		int number[] = {9, 17, 18, 7, 1, 4, 2, 13, 20, 3};
		int largest = 0;
		int secLargest = 0;
		
		System.out.print("Array: ");
		for (int i = 0; i<number.length; i++) {
			System.out.print(number[i] + " ");
		}
		
		//for loop to find second largest element in array
		for(int j = 0; j<number.length; j++) {
			if(number[j]>largest) {				
				secLargest = largest;
				largest = number[j];
			}
			else if(number[j]>secLargest) {
				secLargest = number[j];
			}
		}
		
		System.out.println("\nSecond largest element is: " + secLargest);
		
	}
	
	

}
