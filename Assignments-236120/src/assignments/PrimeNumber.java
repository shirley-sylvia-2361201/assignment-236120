package assignments;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		
		/*Check the prime number	
		  Prime number: - Divisible by 1 or itself
		  				- A whole number greater than 1*/
		
		boolean prime = true; //assume prime is true
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter a number to check if the number is prime: ");
		int number = sc.nextInt();
		
		//if else statement to check if user enter positive integer
		if(number > -1) {
			
			/*--for loop to check if the number is prime
			  --for loop start from two because prime number should be greater than one
			  --for loop is run to check if the number is divisible by any number in the range of 2 to n( n is user input number)*/
			for(int i=2; i<number; i++) {
				if(number%i == 0) {//check remainder if divisible by 0 means that it's not a prime number
					prime = false;
					break;
				}
			}
			
			if (prime == true) {
				System.out.println(number + " is a prime number.");
			}
			else {
				System.out.println(number + " is not a prime number.");
			}
		
		} else {
			System.out.println("Th number " + number + " is invalid!");
		}
		
	}

}
