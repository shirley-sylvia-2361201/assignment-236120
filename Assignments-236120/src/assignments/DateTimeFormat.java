package assignments;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;
import java.util.Scanner;
import java.text.ParseException;

class DateTimeFormat {
	
   public static void main(String[] args) throws ParseException {
	   
	   Scanner sc = new Scanner(System.in);
	   
	   System.out.println("Enter 12 hour format time: ");
	   String s = sc.nextLine();
	   
	   DateFormat inFormat = new SimpleDateFormat("hh:mm:ssaa");
	   DateFormat outFormat = new SimpleDateFormat("HH:mm:ss");
	   
	   Date date = null;
	   date = inFormat.parse(s);
	   if(date != null) {
		   String myDate = outFormat.format(date);
		   System.out.println(myDate);
	   }
	   
	   sc.close();
	     
   }
}