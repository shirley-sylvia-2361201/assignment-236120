package assignments;

import java.util.Arrays;

public class DuplicateArray {

	public static void main(String[] args) {
		
		/*Remove duplicate elements in an array*/
		
		int number[] = {10,1,5,9,1,3,2,7,8,9,4,6};	
		Arrays.sort(number); //sort array in ascending order
		
		int result[] = new int[number.length]; //create temporary of same size array size as number
		int k = 0;
		
		for(int i=0; i<number.length-1;i++) {
			if(number[i]!=number[i+1]) { //compare sorted array in ascending order
				result[k++] = number[i]; //if the number compared is different then store number in result
			}
		}		
		result[k++] = number[number.length-1];
		
		for(int j=0;j<k;j++) {
			System.out.print(" " + result[j]);
		}
		
	}

}