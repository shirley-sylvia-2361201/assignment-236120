package assignments;

import java.util.Scanner;

public class ElectricityBillGenerator {
	
	int payment;
	int power, age;
	
	//method to calculate power used
	public void PowerCalculation() {
			
		if (power>=0 && power<=100) {
			System.out.println("\nYou don't have to pay the bill.");
		}
		else if (power>=101 && power<=200) {
			payment = power * 10;
		}
		else if (power>=201 && power<=400) {
			payment = power * 25;
		}
		else if (power>=401 && power<=1000) {
			payment = power * 50;
		}
		else if (power>1000){
			payment = 500000;
			System.out.print("\nThe electric usage surpase monthly usage, your electric will be disconnected.");
		}
		else {
			System.out.println("Invalid Value!");
		}
	}
	
	
	public static void main(String[] args) {
		
		int total = 0;

		Scanner sc = new Scanner(System.in);
		ElectricityBillGenerator generator = new ElectricityBillGenerator();
		
		//prompt user to enter their name, age and power consumption
		System.out.print("Enter Customer Name: ");
		String name = sc.nextLine();
		
		System.out.print("Enter Customer Age: ");
		generator.age = sc.nextInt();
		
		System.out.print("Enter Power Consumption: ");
		generator.power = sc.nextInt();
		
		generator.PowerCalculation();
		
		//if else to check power used
		if (generator.power>=0 && generator.power<=1000) {
			
			if (generator.age>59 && generator.age<80) {
				total = generator.payment - ((generator.payment * 50)/100);
				System.out.println("\nEligible for 50% discount. Price after discount is: " + total + "Rs");
				
			}
			else if (generator.age>79) {
				total = generator.payment - ((generator.payment * 80)/100);
				System.out.println("\nEligible for 80% discount. Price after discount is: " + total + "Rs");
			}
			else {
				total = generator.payment;
				System.out.println("\nTotal you need to pay is: " + total + " Rs");
			}
			
		}		
		else {
			total = generator.payment;
			System.out.println("\nYou need to pay the total penalty of: " + total + " Rs");
		}
				
	}

}
