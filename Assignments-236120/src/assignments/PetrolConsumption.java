package assignments;

import java.util.Scanner;

public class PetrolConsumption {

	public static void main(String[] args) {
		
		int costperlitre = 115;
		int litre, distance;
		double miles = 0;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter type of vehicle: ");
		String vehicle = sc.nextLine().toLowerCase();
		
		System.out.print("Cost of petrol: ");
		int cost = sc.nextInt();
		
		if (vehicle==("car")) {
			litre = cost/costperlitre;
			distance = litre * 8;
			miles = distance / 1.6;
		}
		else {
			litre = cost/costperlitre;
			distance = litre * 20;
			miles = distance / 1.6;
		}
		
		System.out.println("Total petrol in litre: " + litre);
		System.out.println("Distance can be traveled in km: " + distance);
		System.out.println("Distance can be traveled in miles: " + miles);

	}

}
