package assignments;

import java.util.Scanner;

public class LargestNumber {

	public static void main(String[] args) {

		/*Find largest number in group*/
		int number[], size;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter size of array: ");
		size = sc.nextInt();
		
		number = new int[size];
		System.out.println("Enter elements of array: ");
		
		for(int i =0;i<number.length;i++) {
			number[i] = sc.nextInt();
		}
		
		int max = number[0];
		for(int i=1;i<number.length;i++) {
			if(max<number[i]) {
				max = number[i];
			}
		}
		
		System.out.println("\nThe maximum value is: " + max);

	}

}
