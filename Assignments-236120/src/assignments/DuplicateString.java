package assignments;

public class DuplicateString {

	public static void main(String[] args) {
		
		/*Find duplicate characters in String*/
		String str = "everyday";
		
		char[] ch = str.toCharArray(); //convert string to char array		
		for(int i=0;i<str.length();i++) {
			for(int j=i+1;j<str.length();j++) {
				if(ch[i]==ch[j]) {
					System.out.print(" " + ch[j]);
					break;
				}
			}
		}

	}

}
