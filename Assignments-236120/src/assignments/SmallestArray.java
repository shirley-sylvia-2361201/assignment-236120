package assignments;

import java.util.Scanner;

public class SmallestArray {

	public static void main(String[] args) {
		
		/*To find the smallest element in array*/	
		int number[] = new int[5]; //Initialize array of size 5
		Scanner sc = new Scanner(System.in);
		
		//Enter five elements by using 'for' loop
		System.out.println("Enter Array Elements: ");
		for(int i=0;i<number.length;i++) {
			number[i] = sc.nextInt();
		}
		
		//Find the minimum value by using 'for' loop
		int min = number[0]; //Initialize minimum value
		for(int j=1; j<number.length;j++) { 
			if(number[j]<min) {
				min=number[j];
			}
		}
		
		System.out.println("\nThe minimum value is: " + min);
		
	}

}
