package persondetails;

import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ToJson {
	
	public static void main(String[] args) {
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		PersonDetails pd = createmyObject();
	
		try(FileWriter writer = new FileWriter("C:\\Users\\USER\\eclipse-workspace\\persondetails.json")) {
			gson.toJson(pd, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static PersonDetails createmyObject() {
		
		PersonDetails pd = new PersonDetails();
		pd.setName("Glade");
		pd.setAge(25);
		pd.setGender("Female");
		pd.setCity("Rome");
		pd.setCountry("Italy");
		return pd;
		
	}

}
