package persondetails;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import com.google.gson.Gson;

public class ToObject {
	
	public static void main(String[] args) {
		
		Gson gson = new Gson();
		
		try(Reader read = new FileReader("C:\\Users\\USER\\eclipse-workspace\\persondetails.json")) {
			PersonDetails pd = gson.fromJson(read, PersonDetails.class);
			System.out.println(pd);
		} catch(IOException e) {
			e.printStackTrace();
		}
		
	}

}
